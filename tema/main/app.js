function addTokens(input, tokens){
    if((typeof input)!="string")throw new TypeError("Invalid input")
    
    if(input.length<6)throw new TypeError("Input should have at least 6 characters")
    
    tokens.forEach(element => {
        if((typeof element.tokenName)!="string")throw new TypeError("Invalid array format")
    });

    if(input.indexOf("...")==-1)
        return input;
    else{
       var k = 0;
       while(input.indexOf("...") != -1){
           var deSchimbat = '${' + tokens[k].tokenName + '}';
           input = input.replace("...", deSchimbat);
           k++;
       }
       return input;
    }
}

const app = {
    addTokens: addTokens
}

module.exports = app;